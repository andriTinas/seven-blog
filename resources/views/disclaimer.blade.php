@extends('layouts.frontend.app')

@section('title','Disclaimer')
    
@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">
<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>Disclaimer</b></h1>
</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post-wrapper">
                    <h1>Disclaimer</h1>
                    <hr>
                        <p align="justify">
                            Dengan anda telah mengakses Seven Team maka anda telah diangap mengerti dan menyetujui seluruh syarat serta kondisi yang berlaku dalam penggunaan blog atau website ini, sebagaimana yang sudah tecantum dibawah ini: 
                        </p>
                        <br>
                        <p align="justify">
                            Blog atau Website ini saya buat untuk belajar menulis dan share artikel, baik berupa pengalaman maupun pemikiran pribadi ataupun informasi yang bermanfaat untuk pembaca yang dirangkumkan dari blog dan website besar lainnya. Pada setiap tanggal, bulan dan tahun penulisan saya berharap informasi yang tersedia pada blog atau website ini digunakan sebagai rujukan atau referensi saja. Pastinya saya menulis berdasarkan apa yang saya ketahui pada tanggal, bulan dan tahun artikel saja.  
                        </p>
                        <br>
                        <p align="justify">
                            Saya tidak dapat menjamin semua informasi yang disajikan pada blog atau website ini akurat dan lengkap sehingga saya tidak akan bertanggung jawab atas segala kesalahan dan keterlambatan dalam memperbaharui informasi atau segala kerugian yang timbul akibat tindakan yang berkaitan dengan penggunaan informasi yang ada pada blog atau website ini. 
                        </p>
                        <br>
                        <p align="justify">
                            Saya sangat berterima kasih apabila ada pembaca yang berkenan memberitahukan saya tentang ketidak update-an atau ketidak relevanan artikel yang pernah saya tulis di blog atau website ini melalui kolom komentar dan contact yang sudah tersedia.   
                        </p>
                        <br>
                        <p align="justify">
                            Mungkin dalam blog atau website ini akan terdapat beberapa link yang akan menuju pada situs lain yang akan saya gunakan untuk melengkapi informasi tulisan saya pada saat tanggal penulisan dan ketika saat itu masih relevan. Oleh karena itu saya tidak bertanggung jawab atas isi atau perubahan pada konten situs yang saya tautkan. Setiap pembaca blog atau website ini sangat diperbolehkan untuk berkomentar yang relevan dengan catatan komentar yang sudah anda posting anda akan siap untuk bertanggung jawab ketika itu salah. 
                        </p>
                        <br>
                        <p align="justify">
                            Tulisan yang terdapat pada blog atau website ini merupakan pendapat pribadi penulis dan bukan cerminan sikap instansi atau pihak manapun.        
                        </p>
                        <br>
                        <p align="justify">
                                Oke sekali lagi saya ucapkan kepada anda karena telah berkunjung ke Seven Team silahkan berkunjung kembali untuk mendapatkan informasi yang terupdate lainnya.     
                        </p>
                        <br>
                        
                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->
@endsection

@push('js')
    
@endpush