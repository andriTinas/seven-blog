@extends('layouts.frontend.app')

@section('title','About')
    
@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">
<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>About</b></h1>
</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post-wrapper">
                   <h1>Tentang Kami</h1>
                   <hr>
                    <p align="justify">
                        Sebelumnya perkenalkan saya Dinda Budiasih, tinggal di kota pontianak keseharian saya bekerja di sebuah pabrik baju yang menyediakan baju buat anda. Selain itu waktu yang saya miliki kebanyakan Free maka dari itu saya mencoba untuk mejadi seorang blogger. Tujuan saya blogger yaitu menulis tentang apa yang saya ketahui, karena banyak waktu free dikehidupan saya jadi saya bermain game untuk mengisi waktu luang. Maka dari itu ini blog pertama saya yang mengulas tentang game yang saya ketahui dan pernah saya mainkan.
                    </p>
                    <br>
                    <p align="justify">
                        Jujur ini blog pertama yang saya buat agar tulisan yang saya publish bisa dibaca oleh orang banyak, mungkin di dalam blog ini banyak terdapat artikel yang sama di mesin penelusuran Google. Namun blog saya ini tidaklah mengambil konten orang lain melainkan ini konten ori buatan tangan saya sendiri. Saya mencoba menjadi blogger ini yaitu bertujuan untuk mencari uang jajan tambahan dari anda yang membaca artikel yang saya buat. Tujuan saya sudah pasti untuk menjadi patner dari Google Adsense sebagai Publisher. Pastinya saya berharap artikel saya ini sangat berguna bagi anda juga sebagai pengemar gamers di seluruh Indonesia.
                    </p>
                    <br>
                    <p align="justify">    
                        Lumayan dari pada waktu dibuang percuma lebih bagus digunakan untuk menulis konten yang bermanfaat, serta menghasilkan uang tambahan walaupun tak seberapa. Akan tetapi tujuan dari saya yang pertama tadi adalah agar artikel yang saya tuliskan bermanfaat bagi anda para pembaca. Untuk anda jika ada game yang ingin saya Review, anda bisa menulis komentar pada kolom komentar yang sudah tersedia dan juga bisa memberitahukan kepada saya langsung melalui email yang sudah tertulis di Contact.
                    </p>
                    <br>
                    <p align="justify"> 
                        Nah munkin hanya sedikit ini yang dapat saya bagikan tentang saya, jika anda ingin mengenal saya lebih jauh terus saja membaca artikel yang saya publishkan.
                   </p>
                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->
@endsection

@push('js')
    
@endpush