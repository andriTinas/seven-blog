@extends('layouts.frontend.app')

@section('title','Contact')
    
@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">
<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>Contact</b></h1>
</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post-wrapper">
                    <h1>Contact</h1>
                    <hr>
                        <p align="justify">
                                Jika anda memiliki pertanyaan atau keluhan, anda bisa langsung berkomentar pada kolom komentar yang sudah tersedia. Ingat untuk menggunakan kata-kata yang sopan, maka pasti saya akan segan kepada anda. Saya tidak akan pernah melarang jika anda mengkeritik saya jika itu memang benar salah, kemungkinan saya akan senang karena anda telah memperdulikan tulisan yang saya publish. Sekali lagi terima kasih anda telah mengunjungi blog yang saya punyai.
                        </p>
                      
                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->
@endsection

@push('js')
    
@endpush