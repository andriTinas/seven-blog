@extends('layouts.backend.app')
@section('title', 'Add - Post')

@push('css')

    <!-- Sweet Alert Css -->
    <link href="{{asset('assets/backend/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />
 
    <!-- Bootstrap Select Css -->
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

@endpush

@section('content')

<div class="container-fluid">
   
    {!! Form::open(['route'=>'author.post.store','files'=>true]) !!}
    <!-- Advanced Validation -->
        @include('author.post._form')

    
    <!-- #END# Advanced Validation -->
    {!! Form::close()!!}     
</div>


@endsection

@push('js')
        <!-- Select Plugin Js -->
         <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
        <!-- Jquery Validation Plugin Css -->
        <script src="{{asset('assets/backend/plugins/jquery-validation/jquery.validate.js')}}"></script>

        <!-- JQuery Steps Plugin Js -->
        <script src="{{asset('assets/backend/plugins/jquery-steps/jquery.steps.js')}}"></script>

        <!-- Sweet Alert Plugin Js -->
        <script src="{{asset('assets/backend/plugins/sweetalert/sweetalert.min.js')}}"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="{{asset('assets/backend/plugins/node-waves/waves.js')}}"></script>
         <!-- TinyMCE -->
        <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>


        <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
@endpush

@push('script')
@include('layouts.frontend.partial.tinymce')

@endpush