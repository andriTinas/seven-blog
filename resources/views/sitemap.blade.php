@extends('layouts.frontend.app')

@section('title','Sitemap')
    
@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">
<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>Sitemap</b></h1>
</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post-wrapper">
                    <h1>Sitemap</h1>
                    <hr>
                    @foreach ($categories as $item)
                    <div class="list-group">
                        <a href="{{url('/post/category',$item->slug)}}" class="list-group-item active">
                            {{$item->name}}
                        </a>
                       @foreach ($item->posts as $post)
                       <a href="{{url('/post',$post->slug)}}" class="list-group-item ">
                            {{$post->title}}
                        </a>
                       @endforeach
                    </div>
                    <br>
                    @endforeach

                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->
@endsection

@push('js')
    
@endpush