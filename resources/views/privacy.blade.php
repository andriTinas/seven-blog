@extends('layouts.frontend.app')

@section('title','Privacy')
    
@push('css')
<link href="{{asset('assets/frontend/css/auth/styles.css')}}" rel="stylesheet">
<link href="{{asset('assets/frontend/css/auth/responsive.css')}}" rel="stylesheet">
@endpush

@section('content')
<div class="slider display-table center-text">
    <h1 class="title display-table-cell"><b>Privacy</b></h1>
</div><!-- slider -->

<section class="blog-area section">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post-wrapper">
                    <h1>Privacy</h1>
                    <hr>
                    <p align="justify">
                        Kami melakukan privasi anda dengan sangat sungguh-sungguh. Kebijakan privasi ini menggambarkan informasi pribadi apa saja yang dapat kami kumpulkan dengan bagaimana kami menggunakan. 
                    </p>
                    <br>
                    <p align="justify">
                        Semua web server dapat melacak informasi dasar tentang penggunjung, tentunya informasi ini mencakup namun sangat terbatas pada alamat IP, Rincian Browser, Waktu, Halaman Rujukan, dan jumlah pada klik untuk menganalisa kecendrungan. Mengelola situs, melacak gerakan pengguna disekitar lokasi dan mengumpulkan infirmasi demografis. Alamat IP dan Informasi lainnya yang bersifat pribadi tidak terhubung kemanapun. 
                    </p>
                    <br>
                    <p align="justify">
                        Kami menggunakan cookie untuk menyimpan informasi tentang preferensi pengunjung, merekam informasi pengguna tertentu pada halaman akses atau yang pengguna kunjungi, menyesuaikan konten halaman web berdasarkan jenis browser pengunjung atau informasi lainnya yang pengunjung kirimkan melalui browser mereka. 
                    </p>
                    <br>
                    <h6> <b>  Mitra Iklan Pihak Ketiga </b></h6>
                    <br>
                    <p align="justify">
                        Kami bekerja sama dengan mitra iklan pihak ketiga yaitu Google Adsense. Mitra iklan dan pihak ketiga menggunakan cookie, script ataupun web beacon untuk melacak pengunjung ke situs kami untuk menampilkan iklan dan informasi yang berguna lainnya. Pelacakan seperti ini dilakukan langsung oleh pihak ketiga melalui server mereka sendiri dan tunduk pada kebijakan privasi mereka sendiri. 
                    </p>
                    <br>
                    <p align="justify">
                        Anda langsung dapat mengunjungi Google dan Jaringan konten pada bagian privasi di alamat beriku ini : <a href="https://policies.google.com/privacy?hl=id">https://policies.google.com/privacy?hl=id</a>
                    </p>
                    <br>
                    <h6> <b>  Mengontrol Privasi Anda </b></h6>
                    <br>
                    <p align="justify">
                        Perhatikan bahwa anda dapat mengubah pengaturan browser anda untuk mengaktifkan cookies jika anda memiliki privasi. Menonaktifkan cookies untuk semua situs tidak dianjurkan karena dapat menggangu penggunaan dalam beberapa situs. Pilihan terbaik adalah dengan menonaktifkan atau mengaktifkan cookie pada per-situs dasar. Konsultasikan dokumentasi browser anda untuk instruksi bagaimana mekanisme untuk memblockir cookies pelacakan dan lainnya. 
                    </p>
                    <br>
                </div><!-- post-wrapper -->
            </div><!-- col-sm-8 col-sm-offset-2 -->
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->
@endsection

@push('js')
    
@endpush